#holder class for member data in population

class Member
  attr_accessor :fitness, :name, :mutate

  def initialize(fitness,name)
    @fitness = fitness
    @name = name
    @mutate = false
  end

  def clone
    Member.new(self.fitness, self.name.clone)
  end

end