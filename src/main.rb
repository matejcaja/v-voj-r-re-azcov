#main file to run program
require_relative("main_util")

TARGET_STRING = "I have not failed. I've just found 10.000 ways that won't work"
MAX_GENERATIONS = 5000
TEST_REPEATS = 100

#delete everything - if no backup was made, that's a pitty !
`rm test_*`

main_util = Main_Util.new(TARGET_STRING)

TEST_REPEATS.times do |total|
  population = main_util.create_new_population

  puts "New test, generating new file: total iteration #{total}"

  file = File.open("test_#{total}", 'w')
  MAX_GENERATIONS.times do |i|
    is_finished = false
    max_member = Member.new(0,nil)

    population.each do |member|

      if member.fitness >= max_member.fitness
        max_member.fitness = member.fitness
        max_member.name = member.name.clone
      end

      begin
        if member.name.join.eql?(TARGET_STRING)
          is_finished = true
          file.puts "SOLUTION: Generation #{i}, #{member.name} and #{member.fitness}"
        end
      rescue
        puts 'something went wrong'
      end

    end

    break if is_finished
    file.puts "MAX_MEMBER: #{max_member.fitness}, #{max_member.name}, #{i}"
    population = main_util.tournament(population)
  end

  file.close
  population.clear
end
