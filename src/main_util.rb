#all methods for calculations called by main.rb
require_relative("member")


class Main_Util
  attr_accessor :target_string

  POPULATION_SIZE = 60 #size of population in one generation
  ASCII_TABLE_MIN = 32
  ASCII_TABLE_MAX = 127
  GROUP_SIZE = 2 #number of members for tournament
  MUTATION_COUNT = 1 #number of chars for mutation

  def initialize(target_string)
    @target_string = Array.new
    target_string.each_char do |char|
      @target_string << char
    end
  end

  def generate_random_string(length)
    result = Array.new

    length.times do
      result << rand(ASCII_TABLE_MIN...ASCII_TABLE_MAX).chr
    end

    result
  end

  def create_new_population
    population = Array.new

    POPULATION_SIZE.times do
      name = generate_random_string(@target_string.length)
      fitness = calculate_fitness(name)

      population << Member.new(fitness,name)
    end

    population
  end

  def calculate_fitness(name)
    fitness = 0

    (0..@target_string.length-1).each { |i|
      if @target_string[i].eql? name[i]
        fitness+=1
      end
    }

    fitness
  end

  def tournament(population)
    new_population = Array.new

    group = Array.new
    until population.empty?
      group.clear

      until group.length == 2
        random_position = rand(0..POPULATION_SIZE-1)
        unless population[random_position].nil?
          group << population[random_position].clone
          population.delete_at(random_position)
        end
      end

      result = start_tournament(group)

      result.each do |member|
        new_population << member
      end
    end

    new_population
  end

  def start_tournament(group)
    if group[0].fitness > group[1].fitness
      group[1] = group[0].clone
      group[1].mutate = true
    else
      group[0] = group[1].clone
      group[0].mutate = true
    end

    group.each do |member|
      if member.mutate
        member.name = perform_mutation(member.name)
        member.fitness = calculate_fitness(member.name)
        member.mutate = false
      end
    end

    group
  end

  def perform_mutation(name)
    MUTATION_COUNT.times do
      name[rand(name.length-1)] = generate_random_string(1)[0]
    end

    name
  end
end